{
  const {
    html,
  } = Polymer;
  /**
    `<cells-gema-list-documents>` Description.

    Example:

    ```html
    <cells-gema-list-documents></cells-gema-list-documents>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-gema-list-documents | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsGemaListDocuments extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'cells-gema-list-documents';
    }

    static get properties() {
      return {
        documents: {
          type: Array,
          value: ()=>[],
          notify: true
        },
        tsec: {
          type: String
        },
        loading: {
          type: Boolean,
          value: false
        },
        mysrc: {
          type: String
        }
      };
    }

    static get template() {
      return html `
      <style include="cells-gema-list-documents-styles cells-gema-list-documents-shared-styles"></style>
      <slot></slot>      
      <div class="container" hidden$="[[loading]]">
        <template is="dom-repeat" items="[[documents]]" as="document">
          <p><bbva-list-link-icon icon-left="coronita:pdf" icon-right="" on-click="_handlerEvent" 
          data-information$="[[document.information]]">[[document.heading]]</bbva-list-link-icon></p> 
        </template>
      </div>
      <div hidden$="[[!loading]]" id="containerpdf">
        <cells-file-reader src="{{mysrc}}"></cells-file-reader>
      </div>
      `;
    }

    _handlerEvent(e) {
      let mydata;
      let myHeaders = new Headers();
      myHeaders.append('Content-Type', 'application/json');
      myHeaders.append('tsec', this.tsec);
      let request = new Request('https://t-glomo-mx.prev.grupobbva.com/SRVS_A02/business-documents/v0/business-documents?product.id=MyBusiness', {
        method: 'POST',
        headers: myHeaders,
        body: e.target.dataset.information
      });
     
      var self = this; //alternativa utiliza arrow functions

      fetch(request)
        .then(function(response) {
          return response.text();
        }).then(function(data) {
          //obtener unicamente el contenido pdf
          mydata = data.substr(data.indexOf('>'),data.lastIndexOf('<metadata>'));
          mydata = mydata.substr(1,mydata.indexOf('--')-1);
          console.log(mydata);
          //decodificación
          let binary = atob(mydata);
          let len = binary.length;
          let buffer = new ArrayBuffer(len);
          let view = new Uint8Array(buffer);
          for (let i = 0; i < len; i++) {
            view[i] = binary.charCodeAt(i);
          }

          let blob = new Blob( [view], { type: "application/pdf" });
          let url = URL.createObjectURL(blob);
          //document.location.assign(url); // Sin problemas se muestra el pdf
          self.mysrc = url; //bindeo a atributo src de cells-file-reader
          self.loading = true; //muestra visor
               
        }).catch(function(error) {
          return console.error(error);
        });


      
          
         

    }

  


  }

  customElements.define(CellsGemaListDocuments.is, CellsGemaListDocuments);
}